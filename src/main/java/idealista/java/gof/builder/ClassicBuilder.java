package idealista.java.gof.builder;

public class ClassicBuilder {
    static class ABuilder {
        static StepOne build(final  Integer someInt) {
            return someText -> someBool -> someInt + " num " + someText  + " Text " + someBool;
        }
    }

    interface StepOne {
        StepTwo and( final String someText);
    }

    interface StepTwo {
        String and ( final Boolean someBool);
    }

    public static void main(String[] args) {
        System.out.println(
                ABuilder.build(1).and("asdf").and(false)
        );
    }
}
