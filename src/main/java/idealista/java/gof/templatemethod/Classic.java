package idealista.java.gof.templatemethod;

/**
 * Created by jdvr on 29/11/16.
 */
public class Classic {


    public static abstract class AbstractResourceManipulatorTemplate {
        protected Resource resource;

        private void openResource() {
            resource = new Resource();
        }

        protected abstract void doSomethingWithResource();

        private void closeResource() {
            resource.dispose();
            resource = null;
        }

        public void execute() {
            openResource();
            try {
                doSomethingWithResource();
            } finally {
                closeResource();
            }
        }
    }

    public static class ResourceUser extends AbstractResourceManipulatorTemplate {
        @Override
        protected void doSomethingWithResource() {
            resource.useResource();
        }
    }

    public static class ResourceEmployer extends AbstractResourceManipulatorTemplate {
        @Override
        protected void doSomethingWithResource() {
            resource.employResource();
        }
    }

    public static void main(String[] args) {
        new ResourceUser().execute();
        new ResourceEmployer().execute();
    }
}
